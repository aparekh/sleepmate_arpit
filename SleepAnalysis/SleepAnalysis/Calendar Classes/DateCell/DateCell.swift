//
//  DateCell.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 28/07/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateCell: JTACDayCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var selectedView: UIView!

    @IBOutlet weak var dotView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      
    }
    
    func updateUI(){
        
        self.dotView.layer.cornerRadius = self.dotView.frame.width/2.0
        self.dotView.layer.masksToBounds = true
    }

}
