//
//  DateHeader.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 20/10/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import Foundation


import UIKit
import JTAppleCalendar

class DateHeader: JTACMonthReusableView  {
    @IBOutlet var monthTitle: UILabel!
    
    @IBOutlet weak var weekDayHeaderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.monthTitle.backgroundColor = Constants.Colors.ThemeColor
        self.monthTitle.textColor = Constants.Colors.SecondaryColor
        
        self.weekDayHeaderView.backgroundColor = Constants.Colors.ThemeColor
    }
}
