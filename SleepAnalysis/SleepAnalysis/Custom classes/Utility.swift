//
//  Utility.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 28/10/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import Foundation


class Utility: NSObject {

    static let shared = Utility()
    
    let commonDateFormatter = DateFormatter()

    
}

enum InputDateFormat: String {
    
    case ddMMyyyyDateFormat = "dd-MM-yyyy"
    case fullDateFormat = "dd-MM-yyyy HH:mm:ss"
    
}

enum OutputDateFormat: String {
    
    case ddMMyyyyDateFormat = "dd-MM-yyyy"
    case fullDateFormat = "dd-MM-yyyy HH:mm:ss"
    case onlyTime = "hh:mm a"
    
}


extension String {
    
    func convertToString(inputDateFormat: InputDateFormat, outputDateFormat: OutputDateFormat) -> String? {

        Utility.shared.commonDateFormatter.dateFormat = inputDateFormat.rawValue
        
        if let date = Utility.shared.commonDateFormatter.date(from: self) {
            
            Utility.shared.commonDateFormatter.dateFormat = outputDateFormat.rawValue

            let string = Utility.shared.commonDateFormatter.string(from: date)

            print("String to assing: \(string)")
            return string
            
        }else {

            print("String not assigning as it is nil")
            return nil
            
        }

    }
 
    func stringToDate(inputDateFormat: InputDateFormat) -> Date? {
        
        let dateFormat = inputDateFormat.rawValue
        
        Utility.shared.commonDateFormatter.dateFormat = dateFormat

         Utility.shared.commonDateFormatter.timeZone = .current
        
        let date = Utility.shared.commonDateFormatter.date(from: self)

        return date
    }
    
    
    
}

extension Date {
    
    
    func convertToString(outputDateFormat: OutputDateFormat) -> String {
        
        let dateFormat = outputDateFormat.rawValue
        Utility.shared.commonDateFormatter.dateFormat = dateFormat
        let dateString = Utility.shared.commonDateFormatter.string(from: self)
        return dateString

    }
    
    var dateStringyyyyMMddHHmmss: String? {
        let dateFormat = "yyyy-MM-dd HH:mm:ss"
        Utility.shared.commonDateFormatter.dateFormat = dateFormat
        return  Utility.shared.commonDateFormatter.string(from: self)
    }
    
    
    func updateHourComponents() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm" // or other format
        var comp = DateComponents()
        let calendar = Calendar.current
        comp.hour = Calendar.current.component(.hour, from: self)
        comp.minute = Calendar.current.component(.minute, from: self)
        comp.timeZone = TimeZone(abbreviation: "GMT")!
        return calendar.date(from: comp)!
    }
    
    func getWeekDayName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE" // or other format
        return dateFormatter.string(from: self)
    }
    
}

extension Date {
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
}
