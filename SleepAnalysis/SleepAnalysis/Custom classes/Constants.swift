//
//  Constants.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 24/11/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import Foundation
import UIKit


struct Constants {

    struct Colors {
        static let ThemeColor = UIColor.init(red: 94.0/255.0, green: 94.0/255.0, blue: 94.0/255.0, alpha: 1.0)
        static let SecondaryColor = UIColor.white
    
    }

}

struct UserDefaultsKeys {
    static let StartDateFirstTime = "StartDateFirstTime"
}
