//
//  AddRecordVC.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 14/04/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import UIKit
import RealmSwift

class AddSleepTimingsVC: UIViewController {
    @IBOutlet weak var viewContent: UIView!
    
    @IBOutlet weak var heightConstantContentView: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var dateLbl: UILabel!
    var selectedDate: Date?
    
    @IBOutlet weak var txtSleepTime: UITextField!
    
    
    @IBOutlet weak var txtWakeUpTime: UITextField!
    @IBOutlet weak var tableViewChart: UITableView!

    var isForUpdate = false
    
    var selectedTimeRecord: TimeRecord?
    var chartQuestionsList = List<ChartQuestion>()
    var selectedWakeUpDate: String?
    var selectedSleepDate: String?
    
    var isWakeUpTimeTxtTapped = false
    
    @IBOutlet weak var dayLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Add Sleep Time"
        self.tableViewChart.tableFooterView = UIView()
        
        self.setUpUI()
        
        let saveBarButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.saveButtonClicked))
        self.navigationItem.rightBarButtonItem = saveBarButton
        
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableViewChart.reloadData()
        
        let y = self.tableViewChart.frame.origin.y
        self.heightConstantContentView.constant = y + CGFloat((self.chartQuestionsList.count * 90))
    }
    
    func setUpUI() {
        
        
    }
    
    
    func loadData(){
        
        ///Date and Time
        self.dateLbl.text  = self.selectedDate?.convertToString(outputDateFormat: .ddMMyyyyDateFormat)
        if let selectedDate = self.selectedDate {
            self.dayLabel.text = selectedDate.getWeekDayName()
        }
        if isForUpdate {
            print("Showing slected time")
            print("selcted dwakeup date: \(self.selectedWakeUpDate ?? "")")
            //WakeUp Time
            self.selectedWakeUpDate = self.selectedTimeRecord?.wakeUpTime
            self.txtWakeUpTime.text = self.selectedWakeUpDate?.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime)
            print("selcted sleep date: \(self.selectedSleepDate ?? "")")
            //Sleep Time
            self.selectedSleepDate = self.selectedTimeRecord?.sleepTime
            self.txtSleepTime.text = self.selectedSleepDate?.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime)
        }
        ///Load chart questions
        
        // 1
        self.chartQuestionsList = selectedTimeRecord?.questions ?? List<ChartQuestion>()
        
        if self.chartQuestionsList.count == 0 {
            ///Add data for first time only
             self.chartQuestionsList = TimeRecord.getChartQuestions()
        }
        
        
    }

    /*
       Selecting date in date picker.
     */
    @IBAction func sleepTimeTxtEditingBegin(_ sender: UITextField) {
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .time
        datePickerView.timeZone = .current
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        
        if sender  == txtWakeUpTime {
            
            datePickerView.minimumDate = self.miniumTimeForWakingUp()
            datePickerView.maximumDate = self.maximumTimeForWakeUp()
            
            self.isWakeUpTimeTxtTapped = true
            
            if txtWakeUpTime.text != "" {
                
                datePickerView.date = self.selectedWakeUpDate?.stringToDate(inputDateFormat: .fullDateFormat) ?? self.miniumTimeForWakingUp() ?? Date()
                    //(txtWakeUpTime.text?.stringToDate()!.updateHourComponents()) ?? Date()
                
            }
            
        }else{
            
            self.isWakeUpTimeTxtTapped = false
        
            datePickerView.minimumDate = self.minimumTimeForSleeping()
            datePickerView.maximumDate = self.maximumTimeForSleeping()
            
            if txtSleepTime.text != "" {

                datePickerView.date = self.selectedSleepDate?.stringToDate(inputDateFormat: .fullDateFormat) ?? self.minimumTimeForSleeping()

            }

        
        }

    }
    /*
        Added method for code seperation.
     */
    func miniumTimeForWakingUp() -> Date {
        
        let date = Date()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: date) as NSDateComponents
        components.hour = 2
        components.minute = 0
        components.second = 0
        return gregorian.date(from: components as DateComponents) ?? Date()
    }
    
    func maximumTimeForWakeUp() -> Date {
        
        let date = Date()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: Date()) as NSDateComponents
        components.hour = 11
        components.minute = 0
        components.second = 0
        
        return  gregorian.date(from: components as DateComponents) ?? Date()
    }
    
    func minimumTimeForSleeping() -> Date {
        
        let date = Date()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: Date()) as NSDateComponents
        //7 pm
        components.hour = 19
        components.minute = 0
        components.second = 0
        
        return  gregorian.date(from: components as DateComponents) ?? Date()
    }
    
    func maximumTimeForSleeping() -> Date {
        
        //Next day's 12 h
        let date = Date()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: Date()) as NSDateComponents
        //11:55 am
        components.hour = 23
        components.minute = 55
        components.second = 0
        
        return  gregorian.date(from: components as DateComponents) ?? Date()
        
    }
    /*
       When picker date finished date selection,
       and assinign dat to variables
     */
    @objc func handleDatePicker(sender: UIDatePicker) {
        
        print("Picker date selction finished")
        print("Textfield updated")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        if isWakeUpTimeTxtTapped {
            
            self.selectedWakeUpDate = sender.date.convertToString(outputDateFormat: .fullDateFormat)
            
            self.txtWakeUpTime.text = dateFormatter.string(from: sender.date)

        }else{
            
            self.selectedSleepDate = sender.date.convertToString(outputDateFormat: .fullDateFormat)
            
            self.txtSleepTime.text =  dateFormatter.string(from: sender.date)
        }

    }

    
    @objc func saveButtonClicked(){
        
        
        guard let selectedDate = selectedDate else {
            print("Date not selected, please select the date.")
            return
        }

        let realm = try! Realm()

        
        if let timeRecord = selectedTimeRecord {
            
            print("Updating time record")
            
            try! realm.write() { // 2
                
//                let timeRecord = TimeRecord()
                
                //Selected Date: 24-11-2019 date format
                timeRecord.dateRecord = selectedDate.convertToString(outputDateFormat: .ddMMyyyyDateFormat)
                
                timeRecord.sleepTime = self.selectedSleepDate ?? ""
                //txtSleepTime.text ?? ""
                
                timeRecord.wakeUpTime = self.selectedWakeUpDate ?? ""
                //txtWakeUpTime.text ?? ""
                
                
                self.navigationController?.popViewController(animated: true)
                
            }

        }else{
            
            print("Add new time record")
            try! realm.write() { // 2
                
                let timeRecord = TimeRecord()
                
                //Selected Date: 24-11-2019 date format
                timeRecord.dateRecord = selectedDate.convertToString(outputDateFormat: .ddMMyyyyDateFormat)
                
                timeRecord.sleepTime = self.selectedSleepDate ?? ""
                //txtSleepTime.text ?? ""
                
                timeRecord.wakeUpTime = self.selectedWakeUpDate ?? ""
                //txtWakeUpTime.text ?? ""
                
                realm.add(timeRecord)
                
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        

        
    }

}



extension AddSleepTimingsVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chartQuestionsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartQuestionCell", for: indexPath) as! ChartQuestionCell
        let chartQuestion = self.chartQuestionsList[indexPath.row]
        cell.lblQuestion.text = chartQuestion.questionText
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
}
