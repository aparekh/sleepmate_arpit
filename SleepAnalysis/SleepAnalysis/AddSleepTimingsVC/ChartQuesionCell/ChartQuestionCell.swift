//
//  ChartQuestionCell.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 17/05/20.
//  Copyright © 2020 Ravi Chokshi. All rights reserved.
//

import UIKit

class ChartQuestionCell: UITableViewCell {

    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
