//
//  MyCalendarVC.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 17/08/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import UIKit
import JTAppleCalendar


class MyCalendarVC: UIViewController {

    @IBOutlet weak var jtCalendarView: JTACMonthView!
    @IBOutlet weak var dateAndTimePicker: UIDatePicker!
    @IBOutlet weak var selectedDateLbl: UILabel!
    @IBOutlet weak var wakeUpTimeTxt: UITextField!
    @IBOutlet weak var sleepTimeTxt: UITextField!
    @IBOutlet weak var updateSleepTimeBtn: UIButton!
    
    var selectedTimeRecord: TimeRecord?
    var selectedDate: Date?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.jtCalendarView.calendarDelegate = self
        self.jtCalendarView.calendarDataSource = self
        
        self.jtCalendarView.scrollDirection = .horizontal
        //Paging
        self.jtCalendarView.scrollingMode = .stopAtEachCalendarFrame
        self.jtCalendarView.showsHorizontalScrollIndicator = false
        
        self.jtCalendarView.scrollToDate(Date(),animateScroll: false)
        self.jtCalendarView.selectDates([Date()])
        // Do any additional setup after loading the view.
        
        self.setUpUI()
        
//        jtCalendarView.register(UINib(nibName: "DateCell", bundle: .main), forCellWithReuseIdentifier: "dateCell")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.jtCalendarView.reloadData()
        
    }

    //MARK: Set Up UI
    func setUpUI() {
        
        self.updateSleepTimeBtn.backgroundColor = Constants.Colors.ThemeColor
        
        self.updateSleepTimeBtn.layer.cornerRadius = 5.0
        self.updateSleepTimeBtn.layer.masksToBounds = true

        
    }
    

    func setNavigationTitle(from date: Date){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MMMM, yyyy"
        
        self.navigationItem.title = dateFormatter.string(from: date)
        
    }
   
    //MARK: UI Methods
    func setUpInputTextFields() {
    }
    
    //MARK: Button Clicks
}

