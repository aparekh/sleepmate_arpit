//
//  MyCalendarVC+CollectionView.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 28/07/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import Foundation
import JTAppleCalendar
import RealmSwift

extension MyCalendarVC: JTACMonthViewDataSource {
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
//        let startDate = formatter.date(from: "2019 01 01")!
//        let endDate = formatter.date(from: "2020 01 01")!
//        formatter.dateFormat = "yyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        var dateComponent = DateComponents()
        dateComponent.year = 1
        
        let year = Double(365 * 24 * 60 * 60)
        let startDate = Date().addingTimeInterval(-year)
        let endDate = Calendar.current.date(byAdding: dateComponent, to: startDate)
        
        return ConfigurationParameters(startDate: startDate,
                                       endDate: endDate!,
                                       generateInDates: .forAllMonths,
                                       generateOutDates: .tillEndOfGrid)
        
    }
//    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy MM dd"
//        let startDate = formatter.date(from: "2019 01 01")!
//        let endDate = Date()
//        return ConfigurationParameters(startDate: startDate, endDate: endDate)
//    }
    
    
}

//MARK: Header
extension MyCalendarVC {
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let formatter = DateFormatter()  // Declare this outside, to avoid instancing this heavy class multiple times.
        formatter.dateFormat = "MMM, yyyy"
        
        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "DateHeader", for: indexPath) as! DateHeader
        header.monthTitle.text = formatter.string(from: range.start)
        return header
    }
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return MonthSize(defaultSize: 100)
    }
    
    
    
}
extension MyCalendarVC: JTACMonthViewDelegate {
  
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DateCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
 
        cell.updateUI()
        
        //#1. Month boundary (Other dates not to display in calendar card)
        if cellState.dateBelongsTo == .thisMonth {
            cell.isHidden = false
//            self.setNavigationTitle(from: date)
        } else {
            cell.isHidden = true
        }
        
        //#2. Set Current date or selected date, and fill the UI
        if self.selectedDate == date  && cellState.isSelected == true {
            print("Selected Date: \(date.convertToString(outputDateFormat: .fullDateFormat))")
            _ = self.findRecord(from: date, shouldSetInUI: true)
        }

        //#3 Showing dot indicator, if date found
        let hasDateFound = self.findRecord(from: date, shouldSetInUI: false)

        if hasDateFound {
            
            print("Date found: shw dot view")
            cell.dotView.isHidden = false
        }else{
            
            print("date not found: hide dot view")
            cell.dotView.isHidden = true
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    //MARK: Did selecte date cell
    func calendar(_ calendar: JTACMonthView, shouldSelectDate date: Date, cell: JTACDayCell?, cellState: CellState,indexPath: IndexPath) -> Bool {
        return true // Based on a criteria, return true or false
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        
        configureCell(view: cell, cellState: cellState)
        
        self.selectedDate = date
        
        _ = self.findRecord(from: date, shouldSetInUI: true)

    }
    
    
    /*
       Checking if selcted date's record found in db,
Select time record and show     */
    func findRecord(from date:  Date, shouldSetInUI: Bool = false) -> Bool {
        
        let dateInString = date.convertToString(outputDateFormat: .ddMMyyyyDateFormat)
        let predicate = NSPredicate(format: "dateRecord like %@", dateInString)
        // 1
        let realm = try! Realm()
        
        let results = realm.objects(TimeRecord.self).filter(predicate)
        
        if results.count > 0 {
            
            print("Timerecord found, showing results")

            /* Slect and show the record
             */
            if shouldSetInUI {
            
                let timeRecord = results[0]
            
                self.setTimeLabelsInTextfield(from: timeRecord)
            
                self.selectedTimeRecord = timeRecord
            
            }
            
            return true
            
        }else {
            
            print("Timerecord not fund")
            
            if shouldSetInUI {
            
                self.selectedTimeRecord = nil
                
                self.setTimeLabelsInTextfield(from: nil)
            
            }
            return false
            
        }
        
    }
    
    /*
      Assign Sleep Time and Wake Up time, from time record.
     */
    func  setTimeLabelsInTextfield(from timeRecord: TimeRecord?) {
        
        if let timeRecord = timeRecord {

            //Note: Please check here String may come as nil, so  using ?? ""
            self.sleepTimeTxt.text = timeRecord.sleepTime.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime) ?? ""
        
            self.wakeUpTimeTxt.text = timeRecord.wakeUpTime.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime) ?? ""

            self.updateSleepTimeBtn.setTitle("Update Sleep Time", for: .normal)

        }else {
            
            self.sleepTimeTxt.text = ""
            
            self.wakeUpTimeTxt.text = ""
            
            self.updateSleepTimeBtn.setTitle("Add Sleep Time", for: .normal)
            
        }
    }

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState,indexPath: IndexPath) {

        configureCell(view: cell, cellState: cellState)

    }
    
    //MARK: Update Button Click
    @IBAction func updateSleepTimeButtonClicked(_ sender: Any) {
        
        let obj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddSleepTimingsVC") as! AddSleepTimingsVC
//             AddSleepTimingsVC(nibName: "AddSleepTimingsVC", bundle: nil)
        
        //Fos showing date in next vc
        obj.selectedDate = self.selectedDate
        
        //If timerecord found then it will send  otherwise it will send nil.
        obj.selectedTimeRecord = self.selectedTimeRecord
        
        if self.selectedTimeRecord != nil {
            
            obj.isForUpdate = true
        }
//        self.modalPresentationStyle = .overCurrentContext
//        self.present(obj, animated: true, completion: nil)
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}

//MARK: inDates/monthDates/outDates
/*
    To differenciate current showing months with,
    previous month date, adn next month date.
 */
extension MyCalendarVC {
    
    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
        cell.dateLabel.text = cellState.text
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)

    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.textColor = UIColor.black
        } else {
            cell.dateLabel.textColor = UIColor.gray
        }
    }
    
    //MARK: HAndle cell selectedd
    func handleCellSelected(cell: DateCell, cellState: CellState) {
        if cellState.isSelected {
            cell.selectedView.layer.cornerRadius =  13
            cell.selectedView.isHidden = false
        } else {
            cell.selectedView.isHidden = true
        }
    }
    
}



