//
//  TimeRecord.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 20/10/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import Foundation
import RealmSwift

class TimeRecord: Object {
    
    @objc dynamic var dateRecord = ""
    @objc dynamic var sleepTime = ""
    @objc dynamic var wakeUpTime = ""
    var questions = List<ChartQuestion>()
    
    func addQuestionsFirstTime() {
        
        // Get the default Realm
        let realm = try! Realm()

        let chartQuestion1 = ChartQuestion(indexNo: 0, questionText: "3:30 O'Clock Good Morning")
        questions.append(chartQuestion1)
        let chartQuestion2 = ChartQuestion(indexNo: 1, questionText: "4:00 to 4:45 Amritvela Yog")
        questions.append(chartQuestion2)
        let chartQuestion3 = ChartQuestion(indexNo: 2, questionText: "6:00 to 6:15 Gujarat Zon Star Par Snagathit Yog")
        questions.append(chartQuestion3)
//        let chartQuestion1 = ChartQuestion(indexNo: 0, questionText: "3:30 O'Clock Good Morning")
//        questions.append(chartQuestion1)
//        let chartQuestion1 = ChartQuestion(indexNo: 0, questionText: "3:30 O'Clock Good Morning")
//        questions.append(chartQuestion1)
//        let chartQuestion1 = ChartQuestion(indexNo: 0, questionText: "3:30 O'Clock Good Morning")
//        questions.append(chartQuestion1)
        
        // Persist your data easily
        try! realm.write {
            realm.add(chartQuestion1)
            realm.add(chartQuestion2)
            realm.add(chartQuestion3)

        }
        
    }
    
    class func getChartQuestions() -> List<ChartQuestion> {
        
        let questions = List<ChartQuestion>()
        let chartQuestion1 = ChartQuestion(indexNo: 0, questionText: "3:30 O'Clock Good Morning")
        questions.append(chartQuestion1)
        let chartQuestion2 = ChartQuestion(indexNo: 1, questionText: "4:00 to 4:45 Amritvela Yog")
        questions.append(chartQuestion2)
        let chartQuestion3 = ChartQuestion(indexNo: 2, questionText: "6:00 to 6:15 Gujarat Zon Star Par Snagathit Yog")
        questions.append(chartQuestion3)
        return questions
    }
    
}

