//
//  ChartQuestion.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 17/05/20.
//  Copyright © 2020 Ravi Chokshi. All rights reserved.
//

import Foundation
import RealmSwift

class ChartQuestion: Object {
    
    @objc dynamic var indexNo: Int = -1
    @objc dynamic var questionText = ""
    @objc dynamic var answer = false
    
    convenience init(indexNo: Int, questionText: String) {
        print("init")
        self.init()
        self.indexNo = indexNo
        self.questionText = questionText
    }
}
