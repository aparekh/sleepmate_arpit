//
//  LinearTableViewController.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 15/12/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import UIKit
import RealmSwift

class LinearTableViewController: UITableViewController {

    @IBOutlet weak var linerTableView: UITableView!
    
    var timeRecords: [TimeRecord] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        self.title = "Linear data"
        
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.fetchTimeRecords()

    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeRecords.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "LinearTableViewCell", for: indexPath) as! LinearTableViewCell

//        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        // Configure the cell...
        
        let timeRecord = self.timeRecords[indexPath.row]
        
        let dateComponents = timeRecord.dateRecord.components(separatedBy: " ")
        if dateComponents.count > 0 {
         
            cell.dateOfTimeRecord.text = dateComponents[0]
        }
        
        cell.sleepTimeLbl.text = timeRecord.sleepTime.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime) ?? ""
        
        cell.wakeUpTimeLbl.text = timeRecord.wakeUpTime.convertToString(inputDateFormat: .fullDateFormat, outputDateFormat: .onlyTime) ?? ""
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55.0
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LinearTableViewController {
    

    func fetchTimeRecords() {
        
        let realm = try! Realm()
        
        let results = realm.objects(TimeRecord.self)
        self.timeRecords = Array(results).sorted(by: { (timeRecord1, timeRecord2) -> Bool in
            timeRecord1.dateRecord.stringToDate(inputDateFormat: .fullDateFormat) ?? Date() > timeRecord2.dateRecord.stringToDate(inputDateFormat: .fullDateFormat) ?? Date()
        })
        
        self.tableView.reloadData()
        
    }
    
    
    
}
