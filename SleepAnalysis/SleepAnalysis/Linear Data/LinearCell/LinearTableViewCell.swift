//
//  LinearTableViewCell.swift
//  SleepAnalysis
//
//  Created by Ravi Chokshi on 15/12/19.
//  Copyright © 2019 Ravi Chokshi. All rights reserved.
//

import UIKit

class LinearTableViewCell: UITableViewCell {

    @IBOutlet weak var wakeUpTimeLbl: UILabel!
    @IBOutlet weak var dateOfTimeRecord: UILabel!
    @IBOutlet weak var sleepTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
